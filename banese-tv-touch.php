<! DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Getinfo - Portfólio</title>
        <link rel="stylesheet" href="css/tv.css">
        <html lang="pt-br">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <script src="js/jquery-2.2.0.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,100' rel='stylesheet' type='text/css'> 
        <link rel="stylesheet" href="lightbox/css/lightbox.min.css">
    </head>
    <body>
        <?php include('navbar.php') ?>
    
        <div id="Corpotop">
            <header>
                <img id="cabecalho_desktop" src="img/tv/topo.png">
            </header>
        </div>

        <div id="conteudo">
            <section>
                <a href="index.php"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"><p>Voltar</p></span></a>
                <div id="clienteproduto">
                    <p>
                        Cliente: BANESE - Banco do Estado de Sergipe S/A
                    </p>
                    <p>
                        Produto: Banese TV Touch
                    </p>
                </div>
                <div id="assunto">

                    <aside>
                        <div  id="row">
                            <a class="example-image-link" href="img/tv/01.png" data-lightbox="example-set" data-title="TV TOUCH"><img class="example-image" src="img/tv/01-mini.png" alt=""/> <h3>Clique para ampliar</h3></a>
                            
                        </div>
                    </aside>

                    <div id="texto">                    
                        <p>O Banese TV Touch é uma aplicação que funciona em um televisor na parte externa da agência do Banese no Shopping Jardins. Os clientes do banco, podem ser informar sobre as categorias Supremo e Seleto, além de visualizar uma galeria de fotos e vídeos, e obter informações sobre o próprio Banese.</p>
                        <div id="margin_footer" style="padding-bottom: 70px"></div>
                    </div>
                    
                </div>

                        <div  id="row-mobile">
                            
                            <a class="example-image-link2" href="img/tv/01.png" data-lightbox="example-set2" data-title="TV TOUCH"><img class="example-image2" src="img/tv/01-mini.png" alt=""/><h3>Clique para ampliar</h3></a>
                            
                        </div>
                
            </section>
        </div>
        <?php include('footer.php') ?>
        <script src="lightbox/js/lightbox-plus-jquery.min.js"></script>
    </body>
</html>