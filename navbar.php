<div id="bar-green"></div>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="margin-navbar">
            <div class="navbar-header">
                <a class="navbar-brand" href="http://getinfo.net.br/">
                    <img alt="Brand" src="http://getinfo.net.br/wp-content/themes/pearl-wp/layout/images/logo.png">
                </a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://getinfo.net.br/">Home</a></li>
                    <li><a href="http://getinfo.net.br/#quem-somos">Sobre</a></li>
                    <li><a href="http://getinfo.net.br/#treinamentos">Treinamentos</a></li>
                    <li><a href="http://getinfo.net.br/portfolio">Portfólio</a></li>
                    <li><a href="http://getinfo.net.br/#qlik">Qlik</a></li>
                    <li><a href="http://getinfo.net.br/#fale-conosco">Contato</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>