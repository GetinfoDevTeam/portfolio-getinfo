<! DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Getinfo - Portfólio</title>
        <html lang="pt-br">
        <link rel="stylesheet" href="css/riomar.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <script src="js/jquery-2.2.0.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,100' rel='stylesheet' type='text/css'> 
        <link rel="stylesheet" href="lightbox/css/lightbox.min.css">
    </head>
    <body>
        <?php include('navbar.php') ?>
        
        <div id="Corpotop">
            <header>
                <img id="cabecalho_desktop" src="img/riomar/topo.png">
            </header>
        </div>

        <div id="conteudo">
            <section>
                <a href="index.php"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"><p>Voltar</p></span></a>
                <div id="clienteproduto">
                    <p>
                        Cliente: Shopping Riomar
                    </p>
                    <p>
                        Produto: App Riomar Namorados 
                    </p>
                </div>
                <div id="assunto">

                    <aside>
                        <div  id="row">
                            <a class="example-image-link" href="img/riomar/01.png" data-lightbox="example-set" data-title="RIOMAR"><img class="example-image" src="img/riomar/01-mini.png" alt=""/><h3>Clique para ampliar</h3></a>
                        </div>
                    </aside>

                    <div id="texto">
                        <p>
                            Para comemorar o Dia dos Namorados de 2015, desenvolvemos um aplicativo para Facebook para gerar interatividade, engajamento e incrementar o fluxo de pessoas no cliente.
                        </p>
                    </div>
                    <div id="texto">
                        <p>
                            O app permitiu que os usuários fizessem o upload de uma foto, com a possibilidade de incluir um pequeno texto para homenagear a pessoa amada, a partir da frase "Amor preciso dizer que...". As fotos recebidas em nosso servidor foram impressas no formato de imã de geladeira e ficaram expostas no hall do Shopping Riomar para que os clientes retirassem e presenteassem o seu amor.
                        </p>
                    </div>
                    <div id="texto">
                        <p>
                            O app foi desenvolvido em PHP com integração ao Facebook pelo SDK em JS usando o banco de dados MySQL hospedado em nosso servidor.
                        </p>
                    </div>
                    <div id="texto">
                        <p>
                            Mais de 700 fotos foram impressas e a estratégia de divulgação da campanha implementada pela Behold Social Media alcançou mais de 71.800 pessoas nas redes sociais do cliente.
                        </p>
                    </div>
                    <div id="margin_footer" style="padding-bottom: 200px"></div>

                </div>

                        <div  id="row-mobile">
                            <a class="example-image-link2" href="img/riomar/01.png" data-lightbox="example-set2" data-title="RIOMAR"><img class="example-image2" src="img/riomar/01-mini.png" alt=""/><h3>Clique para ampliar</h3></a>
                        </div>
                
            </section>
        </div>
        <script src="lightbox/js/lightbox-plus-jquery.min.js"></script>
        <?php include('footer.php') ?>
    </body>
</html>