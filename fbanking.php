<! DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Getinfo - Portfólio</title>
    <html lang="pt-br">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/banese.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="lightbox/css/lightbox.min.css">
</head>

<body>
    <?php include('navbar.php') ?>
    
    <div id="Corpotop">
        <header>
            <img id="cabecalho_desktop" src="img/banese/topo.png">
        </header>
    </div>

    <div id="conteudo">
        <section>
            <a href="index.php"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"><p>Voltar</p></span></a>
            <div id="clienteproduto">
                <p>
                    Cliente: BANESE - Banco do Estado de Sergipe S/A
                </p>
                <p>
                    Produto: FBanking
                </p>
            </div>
            <div id="assunto">

                <aside>
                    <div id="row">
                        <a class="example-image-link" href="img/banese/01.png" data-lightbox="example-set" data-title="BANESE"><img class="example-image" src="img/banese/01-mini.png" alt="" />
                            <h3>Clique para ampliar</h3>
                        </a>
                    </div>
                </aside>

                <div id="texto">
                    <p>O mais novo canal de relacionamento do Banese. Com a aplicação, inúmeros serviços, podem ser solicitados pelo Facebook, tornando o processo ágil, seguro e cômodo! Basta escolher o produto, preencher o formulário e o banco entrará em contato.</p>
                    </br>
                    <p>O app oferece 13 serviços:</p>
                    </br>
                    <p>Abra Sua Conta</p>
                    <p>Crédito Pessoa Física</p>
                    <p>Cotação de Seguro Auto</p>
                    <p>Seguro de Acidentes Pessoais</p>
                    <p>Investimento</p>
                    <p>Assistência Residencial</p>
                    <p>Cap + Bônus</p>
                    <p>Cartão de Crédito</p>
                    <p>Consórcio Banese</p>
                    <p>Banese Prev+</p>
                    <p>Negocie Sua Dívida</p>
                    <p>RDC - Depósito Remoto de Cheques</p>
                    <p>Banese Clube+</p>
                    <div id="margin_footer" style="padding-bottom: 300px"></div>
                </div>

            </div>

            <div id="row-mobile">
                <a class="example-image-link2" href="img/banese/01.png" data-lightbox="example-set2" data-title="BANESE"><img class="example-image2" src="img/banese/01-mini.png" alt="" />
                    <h3>Clique para ampliar</h3>
                </a>
            </div>

        </section>
    </div>
    <?php include('footer.php') ?>
    <script src="lightbox/js/lightbox-plus-jquery.min.js"></script>
</body>

</html>