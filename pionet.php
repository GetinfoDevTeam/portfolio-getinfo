<! DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Getinfo - Portfólio</title>
        <html lang="pt-br">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/pionet.css">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <script src="js/jquery-2.2.0.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,100' rel='stylesheet' type='text/css'> 
         <link rel="stylesheet" href="lightbox/css/lightbox.min.css">
    </head>
    <body>
        <?php include('navbar.php') ?>
        
        <div id="Corpotop">
            <header>
                <img id="cabecalho_desktop" src="img/pionet/topo.png">
                
            </header>
        </div>

        <div id="conteudo">
            <section>
                <a href="index.php"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"><p>Voltar</p></span></a>
                <div id="clienteproduto">
                    <p>
                        Cliente: Associação de Ensino e Cultura Pio Décimo
                    </p>
                    <p>
                        Produto: PioNET 
                    </p>
                </div>
                <div id="assunto">
                    <div id="texto">                    
                        <p>Sempre buscando avançar no tocante a tecnologia, a faculdade Pio Décimo disponibiliza o PioNET Mobile para iPhone. Esse app disponibilizará para os alunos da faculdade o acesso à: notas e faltas, horários, avisos e pendências.</p>
                    </div>       
                </div>



                <div id="imagem">
                    <a target="_blank"  href="https://play.google.com/store/apps/details?id=getinfo.pionet&hl=pt_BR&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/pt-br-play-badge.png" /></a>      
                    <a  target="_blank"  href="https://itunes.apple.com/br/app/pionet/id1016965337?mt=8"><img id="button2" src="img/pajucara/button.png"/> </a>
                </div>

                    <div  id="row">
                        <a class="example-image-link" href="img/pionet/01.png" data-lightbox="example-set" data-title="PIONET"><p>Clique para ampliar</p><img class="example-image" src="img/pionet/01-mini.png" alt=""/></a>
                        <a class="example-image-link" href="img/pionet/02.png" data-lightbox="example-set" data-title="PIONET"><img class="example-image" src="img/pionet/02-mini.png" alt=""/></a>
                        <a class="example-image-link" href="img/pionet/03.png" data-lightbox="example-set" data-title="PIONET"><img class="example-image" src="img/pionet/03-mini.png" alt=""/></a>
                        <a class="example-image-link" href="img/pionet/04.png" data-lightbox="example-set" data-title="PIONET"><img class="example-image" src="img/pionet/04-mini.png" alt=""/></a>
                        <a class="example-image-link" href="img/pionet/05.png" data-lightbox="example-set" data-title="PIONET"><img class="example-image" src="img/pionet/05-mini.png" alt=""/></a>
                        <a class="example-image-link" href="img/pionet/06.png" data-lightbox="example-set" data-title="PIONET"><img class="example-image" src="img/pionet/06-mini.png" alt=""/></a>
                        <a class="example-image-link" href="img/pionet/07.png" data-lightbox="example-set" data-title="PIONET"><img class="example-image" src="img/pionet/07-mini.png" alt=""/></a>
                        <a class="example-image-link" href="img/pionet/08.png" data-lightbox="example-set" data-title="PIONET"><img class="example-image" src="img/pionet/08-mini.png" alt=""/></a>
                    </div>


    
            </section>
            <script src="lightbox/js/lightbox-plus-jquery.min.js"></script>
        </div>
        <?php include('footer.php') ?>
    </body>
</html>