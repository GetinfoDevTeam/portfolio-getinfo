<! DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Getinfo - Portfólio</title>
    <html lang="pt-br">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/pajucara.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="lightbox/css/lightbox.min.css">
</head>

<body>
    <?php include('navbar.php') ?>
    
    <div id="Corpotop">
        <header>
            <img id="cabecalho_desktop" src="img/pajucara/topo.png">
            <img id="cabecalho_mobile" src="img/pajucara/cabecalho_mobile.jpg" style="display: none;">
        </header>
    </div>

    <div id="conteudo">
        <section>
            <a href="index.php"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"><p>Voltar</p></span></a>
            <div id="clienteproduto">
                <p>
                    Cliente: Pajuçara Sistema de Comunicação – PSCOM
                </p>
                <p>
                    Produto: App Pajuçara
                </p>
            </div>
            <div id="assunto">
                <div id="texto">
                    <p>O Aplicativo Pajuçara reúne todo o conteúdo multiplataformas do Pajuçara Sistema de Comunicação e oferece o maior conteúdo digital de Alagoas. O app reúne informação e entretenimento, com conteúdo interativo e em tempo real e foi desenvolvido nativamente para os sistemas iOS e Android. O app oferece:</p>
                </div>
                <p>
                    TNH1 Notícias
                </p>
                <p>
                    Pajuçara FM
                </p>
                <p>
                    Pajuçara Arapiraca FM
                </p>
                <p>
                    Transmissão de imagens das Câmeras de Trânsito em tempo real
                </p>
                <p>
                    Pajuçara Eventos
                </p>
                <p>
                    VC Repórter
                </p>
                <p>
                    Redes Sociais
                </p>
                <p>
                    Pajuçara Eventos
                </p>
                <div id="texto2">
                    <p>Lançado em julho de 2015, o primeiro aplicativo de um sistema de comunicação de Alagoas já tem quase 50.000 mil downloads em Android e quase 10 mil downloads em iOS.*</p>
                </div>


            </div>



            <div id="imagem">
                <a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.pajucara&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/pt-br-play-badge.png" />
                </a>
                <a target="_blank" href="https://itunes.apple.com/br/app/pajucara/id987480808?mt=8"><img id="button2" src="img/pajucara/button.png" /> </a>
            </div>

            <div id="row">

                <a class="example-image-link" href="img/pajucara/01.png" data-lightbox="example-set" data-title="PAJUÇARA">
                    <p>Clique para ampliar</p><img class="example-image" src="img/pajucara/01.png" alt="" />
                </a>
                <a class="example-image-link" href="img/pajucara/02.png" data-lightbox="example-set" data-title="PAJUÇARA"><img class="example-image" src="img/pajucara/02.png" alt="" />
                </a>
                <a class="example-image-link" href="img/pajucara/03.png" data-lightbox="example-set" data-title="PAJUÇARA"><img class="example-image" src="img/pajucara/03.png" alt="" />
                </a>
                <a class="example-image-link" href="img/pajucara/04.png" data-lightbox="example-set" data-title="PAJUÇARA"><img class="example-image" src="img/pajucara/04.png" alt="" />
                </a>
                <a class="example-image-link" href="img/pajucara/05.png" data-lightbox="example-set" data-title="PAJUÇARA"><img class="example-image" src="img/pajucara/05.png" alt="" />
                </a>
                <a class="example-image-link" href="img/pajucara/06.png" data-lightbox="example-set" data-title="PAJUÇARA"><img class="example-image" src="img/pajucara/06.png" alt="" />
                </a>
            </div>
        </section>
    </div>
    <?php include('footer.php') ?>
    <script src="lightbox/js/lightbox-plus-jquery.min.js"></script>
</body>

</html>