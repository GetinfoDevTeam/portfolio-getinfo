<! DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Getinfo - Portfólio</title>
    <html lang="pt-br">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,100' rel='stylesheet' type='text/css'>
</head>

<body>
    <?php include('navbar.php') ?>

    <img src="img/index/topo.png">

    <div class="container-fluid campo1">
        <div class="row">
            <div class="col-lg-2 "></div>
            <div class="col-lg-3  campo1texto">
                <h1>PAJUÇARA</h1>
                <h2>Você no centro da notícia.</h2>
                <a href="pajucara.php" class="btn">Saiba mais</a>
            </div>
            <div class="col-lg-5 campo1img">
                <img src="img/index/01.png">
            </div>
            <div class="col-lg-2 ">
            </div>
        </div>
    </div>

    <div class="container-fluid campo2">
        <div class="row">
            <div class="col-lg-3  campo2texto-red">
                <h1>PIONET</h1>
                <h2>Conecte-se ao conhecimento.</h2>
                <a href="pionet.php" class="btn">Saiba mais</a>
            </div>
            <div class="col-lg-2 "></div>
            <div class="col-lg-5 campo2img">
                <img src="img/index/02.png">
            </div>
            <div class="col-lg-2 "></div>
            <div class="col-lg-3  campo2texto">
                <h1>PIONET</h1>
                <h2>Conecte-se ao conhecimento.</h2>
                <a href="pionet.php" class="btn">Saiba mais</a>
            </div>
        </div>
    </div>

    <div class="container-fluid campo3">
        <div class="row">
            <div class="col-lg-2 "></div>
            <div class="col-lg-3  campo3texto">
                <h1>FBANKING</h1>
                <h2>Seu banco mais perto de você.</h2>
                <a href="fbanking.php" class="btn">Saiba mais</a>
            </div>
            <div class="col-lg-7  campo3img">
                <img src="img/index/03.png">
            </div>
            <div class="col-lg-7  campo3img-red">
                <img src="img/index/03.1.png">
            </div>
        </div>
    </div>

    <div class="container-fluid campo4">
        <div class="row">
            <div class="col-lg-4 campo4texto-red">
                <h1>RIOMAR NAMORADOS</h1>
                <h2>Amor, preciso dizer que...</h2>
                <a href="riomar.php" class="btn">Saiba mais</a>
            </div>
            <div class="col-lg-7  campo4img">
                <img src="img/index/04.png">
            </div>
            <div class="col-lg-7  campo4img-red">
                <img src="img/index/04.1.png">
            </div>
            <div class="col-lg-2 "></div>
            <div class="col-lg-4 campo4texto">
                <h1>RIOMAR NAMORADOS</h1>
                <h2>Amor, preciso dizer que...</h2>
                <a href="riomar.php" class="btn">Saiba mais</a>
            </div>
        </div>
    </div>

    <div class="container-fluid campo5">
        <div class="row">
            <div class="col-lg-2 "></div>
            <div class="col-lg-4  campo5texto">
                <h1>BANESE TV TOUCH</h1>
                <h2>Divirta-se e conheça seu banco!</h2>
                <a href="banese-tv-touch.php" class="btn">Saiba mais</a>
            </div>
            <div class="col-lg-6 campo5img">
                <img src="img/index/05.png">
            </div>
        </div>
    </div>
    <?php include('footer.php') ?>
</body>

</html>